## Qu'est-ce que c'est ?

Ceci est un template de projet symfony dockerisé. Il sert à avoir un projet déjà configuré et ainsi pouvoir se mettre directement à coder sans se soucier de la mise en production ou l'installation de l'environnement de développement.

## Comment on s'en sert ?

### 1) Créer un nouveau projet

Pour créer un nouveau projet, il faut : 
- créer un dépot git vide
- cloner ce projet
- supprimer la remote master (git remode remove origin)
- créer une remote origin grâce au dépot vide précédement créé
- push le projet sur le dépôt vide

### 2) Développer ce nouveau projet

Ce nouveau projet que vous venez de créer contiens désormais des outils pour aider le développement.

Le Makefile permet de faciliter de nombreuses choses :
- make deploy-dev : permet de créer un container symfony et un container mariadb lié ensemble. Après cette commande, votre site sera accessible à l'addresse localhost:8000
- make migration : permet de lancer une migration sur l'image symfony (console make:migration) et ainsi déployer les changements apportés à la base de données.